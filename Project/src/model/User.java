package model;

import java.io.Serializable;
import java.sql.Date;

public class User implements Serializable {
	private int id;
	private String loginid;
	private String name;
	private Date birthDate;
	private String password;
	private String createDate;
	private String updateDate;

	public User(String loginid, String name) {
		this.loginid = loginid;
		this.name = name;
	}

	public User(int id, String loginid, String name, Date birthDate, String createDate,
			String updateDate) {
		this.id = id;
		this.loginid = loginid;
		this.name = name;
		this.birthDate = birthDate;

		this.createDate = createDate;
		this.updateDate = updateDate;
	}

	public User(String loginid, String password, String name, Date birthDate,int id) {
         this.id=id;
		this.loginid = loginid;
		this.password = password;
		this.name = name;
		this.birthDate = birthDate;

	}

	public User(String name) {	this.name = name;}

	public User(String password, String name, Date birthDate) {

		this.password = password;
		this.name = name;
		this.birthDate = birthDate;
	}

	public User(int id, String loginid, String name, Date birthDate, String password, String createDate,
			String updateDate) {
		this.id = id;
		this.loginid = loginid;
		this.name = name;
		this.birthDate = birthDate;
		this.password = password;
		this.createDate = createDate;
		this.updateDate = updateDate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLoginid() {
		return loginid;
	}

	public void setLoginId(String loginid) {
		this.loginid = loginid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

}
