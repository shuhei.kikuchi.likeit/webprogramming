package control;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.Userdao;
import model.User;


/**
 * Servlet implementation class UserDetailservlet
 */
@WebServlet("/UserDetailservlet")
public class UserDetailservlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserDetailservlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


		request.setCharacterEncoding("UTF-8");
		HttpSession session=request.getSession();
		User user =(User)session.getAttribute("userinfo") ;
	   if(user==null) {
		 response.sendRedirect("LOGINservlet");
		 return;
	   }
		String id = request.getParameter("id");
		System.out.println(id);
		 Userdao userdao = new Userdao();
			User userinfo = userdao.findByUserInfo(id);
			request.setAttribute("id", userinfo);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_detail.jsp");
			dispatcher.forward(request, response);


	}







	}




