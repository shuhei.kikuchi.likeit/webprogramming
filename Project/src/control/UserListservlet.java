package control;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.Userdao;
import model.User;

/**
 * Servlet implementation class UserListservlet
 */
@WebServlet("/UserListservlet")
public class UserListservlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserListservlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session=request.getSession();
		User user =(User)session.getAttribute("userinfo") ;
	   if(user==null) {
		 response.sendRedirect("UserListservlet");
		 return;}
		// TODO Auto-generated method stubontextPath());
		Userdao userDao = new Userdao();
		List<User> userList = userDao.findAll();
		request.setAttribute("UserList", userList);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_list.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)

			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String NameP = request.getParameter("name");
		String loginidP = request.getParameter("loginid");
		String DateP1= request.getParameter("birthDate1");
		String DateP2 = request.getParameter("birthDate2");
		Userdao userDao = new Userdao();
		List<User> userList = userDao.findSearch(loginidP,NameP,DateP1,DateP2);

		request.setAttribute("UserList", userList);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_list.jsp");
		dispatcher.forward(request, response);

	}
}

