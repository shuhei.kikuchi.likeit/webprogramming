package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import model.User;

public class Userdao {
	public User findByLoginInfo(String loginid, String password) {
		Connection conn = null;
		try {
			String algpassword = passwordAlg(password);
			conn = DBmanager.getConnection();
			String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";
			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, loginid);
			pstmt.setString(2, algpassword);
			ResultSet rs = pstmt.executeQuery();
			if (!rs.next()) {
				return null;
			}
			String loginidData = rs.getString("login_id");
			String nameData = rs.getString("name");
			return new User(loginidData, nameData);

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return null;

	}
	public User findByLoginId(String loginid) {
		Connection conn = null;
		try {

			conn = DBmanager.getConnection();
			String sql = "SELECT * FROM user WHERE login_id = ? ";
			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, loginid);

			ResultSet rs = pstmt.executeQuery();
			if (!rs.next()) {
				return null;
			}
			String loginidData = rs.getString("login_id");
			return new User(loginidData);

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return null;

	}
	public List<User> findAll() {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			conn = DBmanager.getConnection();

			String sql = "SELECT * FROM user WHERE id !=1 ";

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				int id = rs.getInt("id");
				String loginid = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				User user = new User(id, loginid, name, birthDate, password, createDate, updateDate);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;

	}

		public List<User> findSearch(String loginIdP, String NameP ,String DateP1,String DateP2) {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			conn = DBmanager.getConnection();

			String sql = "SELECT * FROM user WHERE id !=1 ";

			if(!loginIdP.equals("")) {
				sql += " AND login_id = '" + loginIdP + "'";
			}

			if(!NameP.equals("")) {
				sql += " AND name LIKE '"+ "%"+ NameP+"%'" ;
			}

			if(!DateP1.equals("")) {
				sql += " AND birth_date >= '"+DateP1+"'";
			}

			if(!DateP2.equals("")) {
				sql +=" AND birth_date <= '"+DateP2+"'";
			}

			System.out.println(sql);

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				int id = rs.getInt("id");
				String loginid = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				User user = new User(id, loginid, name, birthDate, password, createDate, updateDate);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;

	}


	public List<User> findLikeName( String likename) {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			conn = DBmanager.getConnection();

			String sql = "SELECT * FROM user WHERE name LIKE ? ";

			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, "%"+likename+"%");
			ResultSet rs = pstmt.executeQuery();

			while (rs.next()) {
				int id = rs.getInt("id");
				String loginid = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				User user = new User(id, loginid, name, birthDate, password, createDate, updateDate);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;

	}















	public User findByUserInfo(String id) {
		Connection conn = null;
		try {

			conn = DBmanager.getConnection();
			String sql = "SELECT * FROM user WHERE id = ?";
			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, id);

			ResultSet rs = pstmt.executeQuery();
			if (!rs.next()) {
				return null;
			}
			int idData = rs.getInt("id");
			String loginidData = rs.getString("login_id");
			String nameData = rs.getString("name");
			Date birthDateData = rs.getDate("birth_date");
			String createDateData = rs.getString("create_date");
			String updateDateData = rs.getString("update_date");
			return new User(idData, loginidData, nameData, birthDateData, createDateData, updateDateData);

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}

	public void DeleteUserInfo(String id) {
		Connection conn = null;
		try {

			conn = DBmanager.getConnection();
			String sql = "DELETE  FROM user WHERE id = ?";
			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, id);

			pstmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}

	public void NewMemberInfo(String loginid, String password, String name, String birthDate) {
		Connection conn = null;
		try {
			String algpassword = passwordAlg(password);
			conn = DBmanager.getConnection();

			conn = DBmanager.getConnection();
			String sql = " INSERT INTO user(login_id,password,name,birth_Date,create_date,update_date) VALUES (?,?,?,?,NOW(),NOW());";
			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, loginid);
			pstmt.setString(2, algpassword);
			pstmt.setString(3, name);
			pstmt.setString(4, birthDate);
			pstmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void UPdateInfo(String password, String name, String birthDate, String id) {
		Connection conn = null;
		try {
			String algpassword = passwordAlg(password);
			conn = DBmanager.getConnection();
			String sql = " UPDATE user SET password=?,name=?,birth_Date=?,update_date=NOW() WHERE id=?;";
			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, algpassword);
			pstmt.setString(2, name);
			pstmt.setString(3, birthDate);
			pstmt.setString(4, id);
			pstmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void UPdateinfo( String name, String birthDate, String id) {
		Connection conn = null;
		try {

			conn = DBmanager.getConnection();
			String sql = " UPDATE user SET name=?,birth_Date=?,update_date=NOW() WHERE id=?;";
			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, name);
			pstmt.setString(2, birthDate);
			pstmt.setString(3, id);
			pstmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}






	public String passwordAlg(String password) {
		String source = password;

		Charset charset = StandardCharsets.UTF_8;

		String algorithm = "MD5";

		byte[] bytes = null;
		try {
			bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
		} catch (NoSuchAlgorithmException e) {

			e.printStackTrace();
		}
		String result = DatatypeConverter.printHexBinary(bytes);

		System.out.println(result);
		return result;
	}

}
