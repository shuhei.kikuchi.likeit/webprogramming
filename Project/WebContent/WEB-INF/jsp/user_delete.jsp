<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー削除確認</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<nav class="navbar navbar-dark bg-dark">
      <div class="container">
    <a class="navbar-brand" href="#">${userinfo.name} さん</a>
  </div>

    <a href="LOGOUTServlet" class="badge badge-danger">ログアウト</a>
</nav>
    <h1 style=text-align:center> ユーザー削除確認</h1>
</head>
<body>
<div class="text-center">

<p> <a> ログインID：${user.loginid} </a></p><br>
<p>を完全に削除してよろしいでしょうか。</p>
    </div>
  <div class="col-sm-12 text-center ">
     <a href="UserListservlet"  type="button" class="btn btn-secondary btn-lg">キャンセル</a>
<form method="post"action="UserDeleteServlet">
<input type="hidden" value="${user.id}" name="id">
   <button type="submit" class="btn btn-danger btn-lg">ok</button>
</form>
    </div>


</body>
</html>