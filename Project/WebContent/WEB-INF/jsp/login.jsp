<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ログイン画面</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<h1 style=text-align:center> ログイン画面</h1>
 <h3 style=text-align:center class="text-danger">${errMsg}</h3>
<body>

<div class="container">
<form class="form-signin" action="LOGINservlet" method="Post">
  <div class="form-group row">
    <label for="roginid" class="col-sm-3 col-form-label">ログインID</label>
    <div class="col-sm-9">
      <input type="text" class="form-control" name="loginid" >
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-3 col-form-label">パスワード</label>
    <div class="col-sm-9">
      <input type="password" class="form-control" name="password">
    </div>
  </div>


<div class="text-center">

  <button type="submit" class="btn btn-primary" style="align:center">ログイン</button>
</div>


<div>

</div>




    </form>
    </div>

</body>

</html>