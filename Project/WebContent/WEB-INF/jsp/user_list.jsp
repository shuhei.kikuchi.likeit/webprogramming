<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー一覧</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
	integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
	crossorigin="anonymous">
<nav class="navbar navbar-dark bg-dark">
	<div class="container">
		<a class="navbar-brand" href="#">${userinfo.name} さん</a>
	</div>
	<div class="text-right">
		<a href="LOGOUTServlet"
			class="badge badge-danger navbar-link logout-link">ログアウト</a>
	</div>
</nav>



</head>
<h1 style="text-align: center">ユーザー一覧</h1>
<h5>
	<div align="right">
		<a href="NewMemberServlet">新規登録
	</div>
	</a>
</h5>

<body>



	<div class="container">
		<form action="UserListservlet"method="post">
			<div class="form-group row">
				<label for="loginid" class="col-sm-3 col-form-label">ログインID</label>
				<div class="col-sm-9">
					<input type="text" class="form-control" name="loginid">
				</div>
			</div>
			<div class="form-group row">
				<label for="inputPassword" class="col-sm-3 col-form-label">ユーザー名</label>
				<div class="col-sm-9">
					<input type="text" class="form-control"name="name">
				</div>
			</div>
			<div class="form-group row">
				<label for="inputPassword" class="col-sm-3 col-form-label">生年月日</label>
				<div class="col">
					<input type="date" class="form-control" placeholder="年/月/日"  name="birthDate1">
				</div>
				<div class="col">
					<input type="date" class="form-control" placeholder="年/月/日"name="birthDate2">
				</div>
			</div>


		<div class="text-right">
			<button type="submit" class="btn btn-secondary">検索</button>
        </div>
               </form>
		<table class="table table-bordered">
			<thead>
				<tr class="table-primary">
					<th scope="col">ログインID</th>
					<th scope="col">ユーザー名</th>
					<th scope="col">生年月日</th>
					<th scope="col"></th>
				</tr>
			</thead>
			<tbody>

				<c:forEach var="user" items="${UserList}">
					<tr>
						<td>${user.loginid}</td>
						<td>${user.name}</td>
						<td>${user.birthDate}</td>
						<td><a class="btn btn-primary"
							href="UserDetailservlet?id=${user.id}">詳細</a> <c:if
								test="${userinfo.loginid=='admin'}" var="admin">
								<c:if test="${admin}">
									<a class="btn btn-success"
										href="UserUpdateServlet?id=${user.id}">更新</a>
									<a class="btn btn-danger"
										href="UserDeleteServlet?id=${user.id}">削除</a>
								</c:if>
							</c:if>
							<c:if test="${userinfo.loginid==user.loginid}" var="update">
								<c:if test="${update}">
									<a class="btn btn-success"
										href="UserUpdateServlet?id=${user.id}">更新</a>
								</c:if>

							</c:if></td>
					</tr>
				</c:forEach>


			</tbody>
		</table>
	</div>
</body>
</html>