<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー新規登録</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<nav class="navbar navbar-dark bg-dark">
      <div class="container">
    <a class="navbar-brand" href="#">${userinfo.name} さん</a>
  </div>

    <a href="LOGOUTServlet" class="badge badge-danger">ログアウト</a>
</nav>

    <h1 style=text-align:center>ユーザー新規登録 </h1>
    <h3 style=text-align:center class="text-danger">${errMsg}</h3>

<body>
<form action="NewMemberServlet"method="Post">
<div class="container">
  <div class="form-group row">
    <label for="roginID" class="col-sm-2 col-form-label">ログインID</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" name="loginid">
    </div>
    </div>
  <div class="form-group row">
    <label for="inputEmail3" class="col-sm-2 col-form-label-">パスワード</label>
    <div class="col-sm-10">
      <input type="password" class="form-control"name="password" >
    </div>
    </div>
  <div class="form-group row">
    <label for="inputEmail3" class="col-sm-2 col-form-label">パスワード(確認)</label>
    <div class="col-sm-10">
      <input type="password" class="form-control" name="password2">
    </div>
    </div>
  <div class="form-group row">
    <label for="name" class="col-sm-2 col-form-label">ユーザー名</label>
    <div class="col-sm-10">
      <input type="text" class="form-control"name="name" >
    </div>
    </div>
  <div class="form-group row">
    <label for="date" class="col-sm-2 col-form-label">生年月日</label>
    <div class="col-sm-10">
      <input type="date" class="form-control"name="birthDate" >
    </div>
    </div>
       <div class="form-group row ">
    <div class="col-sm-12 text-center ">
      <button type="submit" class="btn btn-primary">登録</button>
    </div>
  </div>

    <p><a href="UserListservlet">戻る</a></p>


      </div>
</body>
</html>